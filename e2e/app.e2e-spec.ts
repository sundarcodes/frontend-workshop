import { GoodReadsAppPage } from './app.po';

describe('good-reads-app App', () => {
  let page: GoodReadsAppPage;

  beforeEach(() => {
    page = new GoodReadsAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
