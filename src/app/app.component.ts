import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  uniqueId = 0;
  listOfBooks = [
    {
        title: 'Refactoring',
        url: 'https://martinfowler.com/books/refactoring.html',
        category: 'Book',
        description: 'Improving the Design of Existing Code',
        isRead: false,
        id: this.uniqueId++
    },
    {
        title: 'Functional Programming',
        url: 'https://maryrosecook.com/blog/post/a-practical-introduction-to-functional-programming',
        category: 'Blog',
        description: 'Practical introduction to functional programming',
        isRead: true,
        id: this.uniqueId++
    },
    {
        title: 'No SQL Distilled',
        url: 'https://martinfowler.com/books/nosql.html',
        category: 'Book',
        description: 'A brief guide to emerging world of polygot persistence',
        isRead: false,
        id: this.uniqueId++
    },
    {
        title: 'Redux',
        url: 'https://code-cartoons.com/a-cartoon-intro-to-redux-3afb775501a6',
        category: 'Blog',
        description: 'A Cartoon introduction to Redux',
        isRead: true,
        id: this.uniqueId++
    },
    {
        title: 'Head First Design Patterns',
        url: 'http://shop.oreilly.com/product/9780596007126.do',
        category: 'Book',
        description: 'Your Brain on Design Patterns',
        isRead: true,
        id: this.uniqueId++
    },
    {
        title: 'Game Programming Patterns',
        url: 'http://gameprogrammingpatterns.com/contents.html',
        category: 'Blog',
        description: 'Design Patterns for Game development',
        isRead: false,
        id: this.uniqueId++
    }
];
}
